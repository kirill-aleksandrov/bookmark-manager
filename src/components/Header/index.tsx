import React from 'react';
import './styles.css';
import { Button } from '../common/Button';

interface HeaderProps {
    onAddClick: React.MouseEventHandler<HTMLButtonElement>;
}

export function Header({ onAddClick }: HeaderProps) {
    return (
        <div className='header'>
            <h1 className='header_title'>Bookmark Manager</h1>
            <Button
                variant='primary'
                onClick={ onAddClick }
            >
                New...
            </Button>
        </div>
    )
}
