import React from 'react';
import { Chip } from './common/Chip';
import { Tag } from '../types/Tag';

interface TagChipProps {
    tag: Tag
}

export function TagChip({ tag }: TagChipProps) {
    return (
        <Chip
            label='Tag'
            color='#a00'
        />
    );
}
