import React, { useState } from 'react';
import { Modal } from './common/Modal';
import { TextField } from './common/TextField';
import { Button } from './common/Button';

interface AddBookmarkModalProps {
    onSubmit: (name: string, url: string) => void
    hideAddBookmarkModal: () => void
}

export function AddBookmarkModal({ onSubmit, hideAddBookmarkModal }: AddBookmarkModalProps) {
    const [name, setName] = useState('');
    const [url, setUrl] = useState('http://');

    return (
        <Modal>
            <>
                <TextField
                    name='name'
                    label='Name'
                    value={ name }
                    onChange={ e => { setName(e.target.value) } }
                />
                <TextField
                    name='url'
                    label='URL'
                    value={ url }
                    onChange={ e => { setUrl(e.target.value) } }
                />

                <Button
                    variant='primary'
                    onClick={ hideAddBookmarkModal }
                >
                    Cancel
                </Button>
                <Button
                    variant='primary'
                    onClick={ () => {
                        onSubmit(name, url);
                        hideAddBookmarkModal();
                    } }
                >
                    Add
                </Button>
            </>
        </Modal>
    )
}
