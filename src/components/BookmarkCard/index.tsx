import React from 'react';
import { Bookmark } from '../../types/Bookmark';
import { Tag } from '../../types/Tag';
import { TagChip } from '../TagChip';
import './styles.css'

interface BookmarkProps {
    bookmark: Bookmark,
    tags: Tag[]
}

export function BookmarkCard(props: BookmarkProps) {
    return (
        <div className='bookmark-card'>
            <h4>
                { props.bookmark.name }
            </h4>
            { props.bookmark.url }
            { props.tags.map(tag => (
                <TagChip tag={ tag }/>
            )) }
        </div>
    )
}
