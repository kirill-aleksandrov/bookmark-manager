import React from 'react';
import { Bookmark } from '../types/Bookmark';
import { BookmarkCard } from './BookmarkCard';

interface BookmarkListProps {
    bookmarks: Bookmark[]
}

export function BookmarkList({ bookmarks }: BookmarkListProps) {
    return (
        <>
            { bookmarks.map(bookmark => (
                <BookmarkCard
                    key={ bookmark.id }
                    bookmark={ bookmark }
                    tags={ [] }
                />
            )) }
        </>
    );
}
