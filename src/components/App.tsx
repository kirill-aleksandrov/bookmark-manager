import React from 'react';
import { Header } from './Header';
import { BookmarkCard } from './BookmarkCard';
import '../styles/variables.css';
import { AddBookmarkModalContainer } from '../containers/AddBookmarkModalContainer';

interface AppProps {
    addBookmarkModalVisible: boolean,
    showAddBookmarkModal: React.MouseEventHandler<HTMLButtonElement>
}

export function App({ addBookmarkModalVisible, showAddBookmarkModal }: AppProps) {
    return (
        <>
            <Header
                onAddClick={ showAddBookmarkModal }
            />

            { addBookmarkModalVisible && (
                <AddBookmarkModalContainer/>
            ) }
        </>
    );
}
