import React from 'react';
import './styles.css';

interface ModalProps {
    children: React.ReactChild
}

export function Modal({ children }: ModalProps) {
    return (
        <div className='modal'>
            <div className='modal__content'>
                { children }
            </div>
        </div>
    );
}
