import React from 'react';

interface ChipProps {
    label: string,
    color: string
}

export function Chip(props: ChipProps) {
    const style = {
        backgroundColor: props.color
    };

    return (
        <div style={style}>
            { props.label }
        </div>
    )
}
