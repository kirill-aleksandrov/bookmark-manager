import React from 'react';
import './styles.css';

interface TextFieldProps {
    name: string
    label: string
    value: string
    onChange: React.ChangeEventHandler<HTMLInputElement>
}

export function TextField({ name, label, value, onChange }: TextFieldProps) {
    return(
        <div>
            <label
                className='text-field__label'
                htmlFor={ name }
            >
                { label }
            </label>
            <input
                name={ name }
                value={ value }
                onChange={ onChange }
            />
        </div>
    )
}
