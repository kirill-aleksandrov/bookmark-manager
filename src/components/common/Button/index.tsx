import React from 'react';
import './styles.css'

interface ButtonProps {
    onClick?: React.MouseEventHandler<{}>
    children: React.ReactChild,
    variant: 'primary'
}

export function Button({ onClick, children, variant }: ButtonProps) {
    return (
        <button
            className={ `button button--${ variant }` }
            onClick={ onClick }
        >
            { children }
        </button>
    )
}
