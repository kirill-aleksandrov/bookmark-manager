import { combineReducers } from 'redux';
import { bookmarksReducer } from './bookmarksReducer';
import { tagsReducer } from './tagsReducer';
import { addBookmarkModalVisibleReducer } from './addBookmarkModalVisibleReducer'

export const rootReducer = combineReducers({
    bookmarks: bookmarksReducer,
    tags: tagsReducer,
    addBookmarkModalVisible: addBookmarkModalVisibleReducer
});

export type AppState = ReturnType<typeof rootReducer>
