import { AddBookmarkModalAction } from '../types/actions/AddBookmarkModalAction';
import { HIDE_ADD_BOOKMARK_MODAL, SHOW_ADD_BOOKMARK_MODAL } from '../constants/addBookmarkModal';

const initialState = false;

export function addBookmarkModalVisibleReducer(state: boolean = initialState, action: AddBookmarkModalAction): boolean {
    switch (action.type) {
        case SHOW_ADD_BOOKMARK_MODAL:
            return true;

        case HIDE_ADD_BOOKMARK_MODAL:
            return false;

        default:
            return state;
    }
}
