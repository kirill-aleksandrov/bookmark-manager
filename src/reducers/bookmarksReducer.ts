import { Bookmark } from '../types/Bookmark';
import { BookmarkAction } from '../types/actions/BookmarkAction';

type BookmarksState = Record<number, Bookmark>;

const initialState: BookmarksState = {};

export function bookmarksReducer(state: BookmarksState = initialState, action: BookmarkAction): BookmarksState {
    return state
}
