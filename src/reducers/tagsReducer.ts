import { Tag } from '../types/Tag';
import { TagAction } from '../types/actions/TagAction';

type TagsState = Record<number, Tag>;

const initialState: TagsState = {};

export function tagsReducer(state: TagsState = initialState, action: TagAction): TagsState {
    return state
}
