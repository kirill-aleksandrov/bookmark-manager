import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { rootReducer } from './reducers';
import { AppContainer } from './containers/AppContainer';

const store = createStore(
    rootReducer,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

const Root = () => (
    <Provider store={ store }>
        <AppContainer/>
    </Provider>
);

ReactDOM.render(
    <Root/>,
    document.getElementById('root')
);
