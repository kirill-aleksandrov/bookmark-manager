import { AddBookmarkModalAction } from '../types/actions/AddBookmarkModalAction';
import { SHOW_ADD_BOOKMARK_MODAL, HIDE_ADD_BOOKMARK_MODAL } from '../constants/addBookmarkModal';

export function showAddBookmarkModal(): AddBookmarkModalAction {
    return {
        type: SHOW_ADD_BOOKMARK_MODAL
    }
}

export function hideAddBookmarkModal(): AddBookmarkModalAction {
    return {
        type: HIDE_ADD_BOOKMARK_MODAL
    }
}
