import { BookmarkAction } from '../types/actions/BookmarkAction';
import { CREATE_BOOKMARK, DELETE_BOOKMARK, UPDATE_BOOKMARK } from '../constants/bookmarks';

export function createBookmark(name: string, url: string): BookmarkAction {
    return {
        type: CREATE_BOOKMARK,
        payload: {
            id: Math.max(Math.random() * 1000000),
            name,
            url,
            tagIds: [],
            createdAt: new Date(Date.now())
        }
    };
}

export function updateBookmark(name: string, url: string): BookmarkAction {
    return {
        type: UPDATE_BOOKMARK,
        payload: {
            id: Math.floor(Math.random() * 1000000),
            name,
            url,
            tagIds: [],
            createdAt: new Date(Date.now())
        }
    };
}

export function deleteBookmark(id: number): BookmarkAction {
    return {
        type: DELETE_BOOKMARK,
        payload: id
    };
}
