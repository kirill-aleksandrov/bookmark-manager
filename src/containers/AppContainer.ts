import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../reducers';
import { App } from '../components/App';
import { showAddBookmarkModal } from '../actions/addBookmarkModalActions';

const mapStateToProps = ({ addBookmarkModalVisible }: AppState) => ({
    addBookmarkModalVisible
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    showAddBookmarkModal: () => dispatch(showAddBookmarkModal())
});

export const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);
