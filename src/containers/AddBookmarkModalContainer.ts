import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../reducers';
import { hideAddBookmarkModal } from '../actions/addBookmarkModalActions';
import { AddBookmarkModal } from '../components/AddBookmarkModal';
import { createBookmark } from '../actions/bookmarksActions';

const mapStateToProps = (state: AppState) => ({});

const mapDispatchToProps = (dispatch: Dispatch) => ({
    hideAddBookmarkModal: () => dispatch(hideAddBookmarkModal()),
    onSubmit: (name: string, url: string) => dispatch(createBookmark(name, url))
});

export const AddBookmarkModalContainer = connect(mapStateToProps, mapDispatchToProps)(AddBookmarkModal);
