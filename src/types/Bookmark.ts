export interface Bookmark {
    id: number,
    name: string,
    url: string,
    tagIds: number[],
    createdAt: Date
}
