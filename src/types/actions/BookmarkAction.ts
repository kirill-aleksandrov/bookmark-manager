import { CREATE_BOOKMARK, DELETE_BOOKMARK, UPDATE_BOOKMARK } from '../../constants/bookmarks';
import { Bookmark } from '../Bookmark';

interface CreateBookmarkAction {
    type: typeof CREATE_BOOKMARK,
    payload: Bookmark
}

interface UpdateBookmarkAction {
    type: typeof UPDATE_BOOKMARK,
    payload: Bookmark
}

interface DeleteBookmarkAction {
    type: typeof DELETE_BOOKMARK,
    payload: number
}

export type BookmarkAction = CreateBookmarkAction | UpdateBookmarkAction | DeleteBookmarkAction
