import { CREATE_TAG, DELETE_TAG, UPDATE_TAG } from '../../constants/tags';
import { Tag } from '../Tag';

interface CreateTagAction {
    type: typeof CREATE_TAG,
    payload: Tag
}

interface UpdateTagAction {
    type: typeof UPDATE_TAG,
    payload: Tag
}

interface DeleteTagAction {
    type: typeof DELETE_TAG,
    payload: Tag
}

export type TagAction = CreateTagAction | UpdateTagAction | DeleteTagAction
