import { SHOW_ADD_BOOKMARK_MODAL, HIDE_ADD_BOOKMARK_MODAL } from '../../constants/addBookmarkModal';

interface ShowAddBookmarkModal {
    type: typeof SHOW_ADD_BOOKMARK_MODAL
}

interface HideAddBookmarkModal {
    type: typeof HIDE_ADD_BOOKMARK_MODAL
}

export type AddBookmarkModalAction = ShowAddBookmarkModal | HideAddBookmarkModal
